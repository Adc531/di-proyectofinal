/**
 * @Project: ProyectoFinal
 * @FileName: Metodos.java
 * @Date: 15 oct 2021
 * 
 * @Author: Ra�l Delgado
 * @Email: raul.delgadosaez2000@gmail.com
 * @GitUser: @Adc531
 * 
 * @Description: 
 * 
**/

package Paquete;

import java.awt.event.*;
import java.io.*;
import java.util.Properties;
import java.util.Scanner;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import org.jsoup.*;
import javax.swing.*;

public class Metodos {
	
	
public static void CargaProgressBar(JProgressBar progressBar, JPanel PanelProgressBar, JPanel PanelLogin, File f, File f1) {
	
	ActionListener updateProBar = new ActionListener() {
	      public void actionPerformed(ActionEvent actionEvent) {
		       int val = progressBar.getValue();
		        progressBar.setValue(val+1);
		        if(progressBar.getValue() == 90 && !f.exists() && !f1.exists()) {
		        	
		        	JOptionPane.showMessageDialog(null, "UPS... Algo ha salido mal!!  ||  Fichero no encontrado" , "Error", JOptionPane.ERROR_MESSAGE);
		        	System.exit(0);
		        	
		        }else if(progressBar.getValue() == 90 && f.exists() && f1.exists()) {
		        	JOptionPane.showMessageDialog(null, "Fichero encontrado" , "Existe", JOptionPane.INFORMATION_MESSAGE);
		        	 progressBar.setValue(val+1);
		        }
		        
		        
		        if(progressBar.getValue() == 100 && f.exists() && f1.exists()) {
		        	PanelProgressBar.setVisible(false);
		        	PanelLogin.setVisible(true);
					 ((Timer) actionEvent.getSource()).stop();
		        	
		        	
		        	
		        }
		      }
		    };
		    Timer time = new Timer(15, updateProBar);
		    time.start();
		      
		    
	}
	
public static void login(Login[] l, JButton btnLogin, File f, JTextField Usuario, JPasswordField Password, JPanel PanelLogin, JPanel PanelConf, JPanel PanelNoticias, JButton btnRegistrarse, JPanel PanelRegister, Configuracion[] Conf) {
	btnLogin.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			try {
				char[] Crts;
				FileReader fr = new FileReader(f);
				BufferedReader BR = new BufferedReader(fr);
				int contador = 0;
				String Linea;
				String User = Usuario.getText();
				Crts = Password.getPassword();
				String Contrase�a = new String(Crts);
				while ((Linea = BR.readLine()) != null) {
						String[] aux = Linea.split(";:;");
						Login aux1 = new Login(aux[0], aux[1], Integer.parseInt(aux[2]), aux[3]); /* 0 es falso y 1 es true*/
						l[contador] = aux1;
						contador++;
					
				}
				
				for(int j = 0 ; j < l.length; j++) {
						
						if (l[j].getUser().equals(User) && l[j].getPassword().equals(Contrase�a)) {
							if (l[j].getConf() == 0) {			
								String linea;	
								FileReader FR = new FileReader(f);
								BufferedReader br = new BufferedReader(FR);
								 while ((linea = br.readLine()) != null) {
									 if(linea.contentEquals(l[j].toString())) {
										 int c = 1;
										 l[j].setConf(c);
										 Conf[j].setId(l[j].getConf());
										 FileWriter fw = new FileWriter(f);
										 BufferedWriter bw = new BufferedWriter(fw);
										 bw.write(l[j].toString());
										 bw.close();
										 c++;
									 }
								 }
								 br.close();
								PanelLogin.setVisible(false);
								PanelConf.setVisible(true);
								
								break;
							}else if (l[j].getConf() >= 1){
								PanelLogin.setVisible(false);
								PanelNoticias.setVisible(true);
								break;
							}
						}
				}
				
				BR.close();
				
					}catch(IOException e) {
							JOptionPane.showMessageDialog(null, "UPS... Algo ha salido mal!!" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);		
							System.exit(1);
					}
	}});
	
	btnRegistrarse.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			PanelLogin.setVisible(false);
			PanelRegister.setVisible(true);
		}
	});
}

	
	public static void Registro(JButton btnRegistro, File f, JPasswordField passwordField, JTextField TFEmail, JTextField TFUsuario, JButton btnVolver, JPanel PanelLogin, JPanel PanelRegister, File f1, File f2) {
		
		btnRegistro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					char[] Crts = passwordField.getPassword();
					String Contrase�a = new String(Crts);
					Registro r = new Registro(TFUsuario.getText(), Contrase�a, TFEmail.getText());
					FileWriter fw = new FileWriter(f, true);
					BufferedWriter bw = new BufferedWriter(fw);
					
					bw.write(r.toString() + r.Email() + "\n");
		
					bw.close();
					
					
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(null, "UPS... Algo ha salido mal!!" + e2.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);		
				}
			}
		});
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PanelRegister.setVisible(false);
				PanelLogin.setVisible(true);
			}
		});
		
	}
	
	public static void Noticias(JTextArea TANoticia, JLabel lblN1, String URL, String html, String periodico) {
		Noticias N = new Noticias(URL, html, periodico);
		TANoticia.setText(N.toString());
		lblN1.setText(N.Periodico());
	}
	public static void ConfNoticias(JButton btnGuardaConf, JRadioButton rdbtnDeportes, JRadioButton rdbtnMarca, JRadioButton rdbtnSport,
			JRadioButton rdbtnF1, JRadioButton rdbtnNacional, JRadioButton rdbtnElPais, JRadioButton rdbtn20Mins, JRadioButton rdbtnInternacional,
			JRadioButton rdbtnMundo, JRadioButton rdbtnNewYorkTimes, JRadioButton rdbtnTheEconomist, JRadioButton rdbtnThetimes, Configuracion[] Conf,
			JButton btnVolver1, File fConf) {
		
		btnGuardaConf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				//DEPORTES
				if(rdbtnDeportes.isSelected()) {
				if(rdbtnMarca.isSelected() && rdbtnSport.isSelected() && rdbtnF1.isSelected()) {					
					Conf [0] = new Configuracion("Deportes", "Marca", "Sport", "F1");
				}else if(rdbtnMarca.isSelected() && !rdbtnSport.isSelected() && !rdbtnF1.isSelected()) {
					Conf [0] = new Configuracion("Deportes", "Marca", "", "");
				}else if(!rdbtnMarca.isSelected() && rdbtnSport.isSelected() && !rdbtnF1.isSelected()) {
					Conf [0] = new Configuracion("Deportes", "", "Sport", "");
				}else if(!rdbtnMarca.isSelected() && !rdbtnSport.isSelected() && rdbtnF1.isSelected()) {
					Conf [0] = new Configuracion("Deportes", "", "", "F1");
				}else if(rdbtnMarca.isSelected() && !rdbtnSport.isSelected() && rdbtnF1.isSelected()) {
					Conf [0] = new Configuracion("Deportes", "Marca", "", "F1");
				}else if(rdbtnMarca.isSelected() && rdbtnSport.isSelected() && !rdbtnF1.isSelected()) {
					Conf [0] = new Configuracion("Deportes", "Marca", "Sport", "");
				}else if(!rdbtnMarca.isSelected() && rdbtnSport.isSelected() && rdbtnF1.isSelected()) {
					Conf [0] = new Configuracion("Deportes", "", "Sport", "F1");
				}else if(!rdbtnMarca.isSelected() && !rdbtnSport.isSelected() && !rdbtnF1.isSelected()) {
					Conf [0] = new Configuracion("Deportes", "", "", "");
				}
				}else if(!rdbtnDeportes.isSelected()) {
					if(!rdbtnMarca.isSelected() && !rdbtnSport.isSelected() && !rdbtnF1.isSelected()) {
						Conf [0] = new Configuracion("Deportes", "", "", "");
					}
				}
				if(rdbtnNacional.isSelected()) {
					if(rdbtnElPais.isSelected() && rdbtn20Mins.isSelected() && rdbtnMundo.isSelected()) {
						Conf [1] = new Configuracion("Nacional", "ElPais", "Mundo", "20Mins");
					}else if(rdbtnElPais.isSelected() && !rdbtn20Mins.isSelected() && !rdbtnMundo.isSelected()) {						
						Conf [1] = new Configuracion("Nacional", "ElPais", "", "");
					}else if(!rdbtnElPais.isSelected() && rdbtn20Mins.isSelected() && !rdbtnMundo.isSelected()) {			
						Conf [1] = new Configuracion("Nacional", "", "Mundo", "");
					}else if(!rdbtnElPais.isSelected() && !rdbtn20Mins.isSelected() && rdbtnMundo.isSelected()) {				
						Conf [1] = new Configuracion("Nacional", "", "", "20Mins");
					}else if(rdbtnElPais.isSelected() && !rdbtn20Mins.isSelected() && !rdbtnMundo.isSelected()) {
						Conf [1] = new Configuracion("Nacional", "", "Mundo", "20Mins");
					}else if(rdbtnElPais.isSelected() && !rdbtn20Mins.isSelected() && !rdbtnMundo.isSelected()) {
						Conf [1] = new Configuracion("Nacional", "ElPais", "Mundo", "");
					}else if(!rdbtnElPais.isSelected() && !rdbtn20Mins.isSelected() && !rdbtnMundo.isSelected()) {
						Conf [1] = new Configuracion("Nacional", "", "", "");
					}
				}else if(!rdbtnNacional.isSelected()) {
					Conf [1] = new Configuracion("Nacional", "", "", "");
				}
				if(rdbtnInternacional.isSelected()) {
					if(rdbtnNewYorkTimes.isSelected() && rdbtnTheEconomist.isSelected() && rdbtnThetimes.isSelected()) {
						Conf [2] = new Configuracion("Internacional", "NYTimes", "Economist", "TheTimes");
					}else if(rdbtnNewYorkTimes.isSelected() && !rdbtnTheEconomist.isSelected() && !rdbtnThetimes.isSelected()) {
						Conf [2] = new Configuracion("Internacional", "NYTimes", "", "");
					}else if(!rdbtnNewYorkTimes.isSelected() && !rdbtnTheEconomist.isSelected() && rdbtnThetimes.isSelected()) {
						Conf [2] = new Configuracion("Internacional", "", "", "TheTimes");
					}else if(!rdbtnNewYorkTimes.isSelected() && rdbtnTheEconomist.isSelected() && !rdbtnThetimes.isSelected()) {
						Conf [2] = new Configuracion("Internacional", "", "Economist", "");
					}else if(rdbtnNewYorkTimes.isSelected() && !rdbtnTheEconomist.isSelected() && rdbtnThetimes.isSelected()) {
						Conf [2] = new Configuracion("Internacional", "NYTimes", "", "TheTimes");
					}else if(rdbtnNewYorkTimes.isSelected() && rdbtnTheEconomist.isSelected() && !rdbtnThetimes.isSelected()) {
						Conf [2] = new Configuracion("Internacional", "NYTimes", "Economist", "");
					}else if(!rdbtnNewYorkTimes.isSelected() && rdbtnTheEconomist.isSelected() && rdbtnThetimes.isSelected()) {						
						Conf [2] = new Configuracion("Internacional", "", "Economist", "TheTimes");
					}else if(!rdbtnNewYorkTimes.isSelected() && !rdbtnTheEconomist.isSelected() && !rdbtnThetimes.isSelected()) {						
						Conf [2] = new Configuracion("Internacional", "", "", "");
					}
				}if(!rdbtnInternacional.isSelected()) {
					Conf [2] = new Configuracion("Internacional", "", "", "");
				}
				try {
					
					FileWriter fw = new FileWriter(fConf, true);
					BufferedWriter bw = new BufferedWriter(fw);
					
					
					for (int i = 0; i < 3; i++) {
						bw.write(Conf[i].toString());
						bw.write("\n");
					}
					bw.close();
				}catch(IOException e1) {}
				btnVolver1.setVisible(true);
			}
			
			
		});
	}
	
	public static void Noticias(File fConf, JTextArea TANoticias, JButton btnMostrarNoticias , JButton btnEnviarEmail, Login[] l, File f, JTextField Usuario, JPasswordField Password){
		
		btnMostrarNoticias.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					File f4 = new File("./Ficheros/Noticias.txt");
					Configuracion[] Conf1 =new Configuracion[5];
					Noticias N;
					int contador = 0;
					FileReader fr = new FileReader(fConf);
					BufferedReader br = new BufferedReader(fr);
					FileWriter fw = new FileWriter(f4, true);
					BufferedWriter bw = new BufferedWriter(fw);
					String Linea;
					while((Linea = br.readLine()) != null ) {
						
						String[] aux = Linea.split(";:;");
						Configuracion aux1 = new Configuracion(aux[0], aux[1], aux[2], aux[3]);
						Conf1[contador] = aux1;
						contador++;
						
					}
					for(int i = 0; i < 5; i++ ) {
						if(Conf1[i].getSeccion().equals("Deportes")) {
							if(Conf1[i].getP1().equals("Marca")) {
								N = new Noticias("https://www.marca.com", "h2 > a", "Marca");
								TANoticias.append(N.toString());
								bw.write(N.Periodico());
							}
							if(Conf1[i].getP2().equals("Sport")) {
								N = new Noticias("https://www.sport.es/es/", "h2 > a", "Sport");
								TANoticias.append(N.toString());
								bw.write(N.Periodico());
							}
							if(Conf1[i].getP3().equals("F1")) {
								N = new Noticias("https://www.elperiodico.com/es/mundial-f1/", "h2 > a", "Formula1");
								TANoticias.append(N.toString());
								bw.write(N.Periodico());
							}
						}
						if(Conf1[i].getSeccion().equals("Nacional")) {
							if(Conf1[i].getP1().equals("ElPais")) {
								N = new Noticias("https://elpais.com/espana/", "h2 > a", "El Pais");
								TANoticias.append(N.toString());
								bw.write(N.Periodico());
							}
							if(Conf1[i].getP2().equals("Mundo")) {
								N = new Noticias("https://www.elmundo.es/espana.html", "a", "El Mundo");
								TANoticias.append(N.toString());
								bw.write(N.Periodico());
							}
							if(Conf1[i].getP3().equals("20Mins")) {
								N = new Noticias("https://www.20minutos.es/nacional/", "header > h1 > a", "20 Minutos");
								TANoticias.append(N.toString());
								bw.write(N.Periodico());
							}
						}
						if(Conf1[i].getSeccion().equals("Internacional")) {
							if(Conf1[i].getP1().equals("NYTimes")) {
								N = new Noticias("https://www.nytimes.com/es/", "h2 > a", "NY Times");
								TANoticias.append(N.toString());
								bw.write(N.Periodico());
							}
							if(Conf1[i].getP2().equals("Economist")) {
								N = new Noticias("https://www.economist.com", "h3> a", "The Economist");
								TANoticias.append(N.toString());
								bw.write(N.Periodico());
							}
							if(Conf1[i].getP3().equals("TheTimes")) {
								N = new Noticias("https://www.thetimes.co.uk", "h3> a", "The Times");
								TANoticias.append(N.toString());
								bw.write(N.Periodico());
							}
						}}
					
				} catch (Exception e1) {
					System.out.println(e1.getMessage());
				}
			}
		});
		btnEnviarEmail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					char[] Crts;
					File f4 = new File("./Ficheros/Noticias.txt");
					FileReader fr = new FileReader(f);
					BufferedReader BR = new BufferedReader(fr);
					FileReader frN = new FileReader(f4);
					BufferedReader BRN = new BufferedReader(frN);
					int contador = 0;
					Noticias[] n = new Noticias[4];
					String Linea;
					String[] correo = new String[4];
					String User = Usuario.getText();
					Crts = Password.getPassword();
					String Contrase�a = new String(Crts);
					while ((Linea = BR.readLine()) != null) {
							String[] aux = Linea.split(";:;");
							Login aux1 = new Login(aux[0], aux[1], Integer.parseInt(aux[2]), aux[3]); /* 0 es falso y 1 es true*/
							l[contador] = aux1;
							correo[contador] = aux[3];
							contador++;
													
					}
					String Email = "noreply832@gmail.com";
					String ClaveCorreo = "Salesianos";
					
					Properties properties = new Properties();
					
					properties.put("mail.smtp.host", "smtp.gmail.com");
					properties.put("mail.smtp.port", "587");
					
					properties.put("mail.smtp.starttls.enable", "true");
					properties.put("mail.smtp.auth", "true");
					properties.put("mail.user", Email);
					properties.put("mail.password", ClaveCorreo);
					
					//Obtener la sesion
					Session session = Session.getInstance(properties, null);
					int aviso = 0;
					try {
						//Crea el cuerpo del mensaje
						MimeMessage mimemessage = new MimeMessage(session);
						//Agregar quien envia el correo
						mimemessage.setFrom(new InternetAddress(Email, "NOREPLY"));
						// Los destinatarios
						InternetAddress[] internetAdress = {new InternetAddress(correo[0])};
						//Agregar los destinatarios al mensaje
						mimemessage.setRecipients(Message.RecipientType.TO, internetAdress);
						//Agregar el asunto al mensaje
						while((Linea = BRN.readLine()) != null) {
							String[] aux = Linea.split(";:;");
							Noticias aux1 = new Noticias(aux[0], aux[1], aux[2]);
							n[contador] = aux1;
							contador++;
						}
						mimemessage.setSubject("Noticias");
						MimeBodyPart mimebody = new MimeBodyPart();
						for(int i = 0; i < n.length; i++) {
							mimebody.setText(n[i].toString());
						}
					}catch(Exception e7) {}
					
					
				}catch(Exception e3) {}
				
			}
		});
	}


	
}
