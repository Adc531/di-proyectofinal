package Paquete;

public class Login  {
	private String User;
	private String Password;
	private String Email;
	private int Conf;
	
	public Login(String user, String password , int conf, String email) {
		User = user;
		Password = password;
		Conf = conf;
		Email = email;
	}
	
	public String getUser() {
		return User;
	}

	public void setUser(String user) {
		User = user;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public int getConf() {
		return Conf;
	}

	public void setConf(int conf) {
		Conf = conf;
	}
	

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	@Override
	public String toString() {
		return User + ";:;" + Password + ";:;" + Email + ";:;" + Conf + ";:;";
	}


	
	
	
}
