package Paquete;

public class Registro {
	
	private String User, Password, Email;
	private int Conf = 0;

	public Registro(String user, String password, String email) {
		User = user;
		Password = password;
		Email = email;
	}

	public String getUser() {
		return User;
	}

	public void setUser(String user) {
		User = user;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public void setId(int id) {
		Conf = id;
	}

	@Override
	public String toString() {
		return User + ";:;" + Password + ";:;" + Conf + ";:;";
	}
	public String Email() {
		return Email + ";:;";
	}
	
	
	
}
