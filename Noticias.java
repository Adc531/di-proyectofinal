/**
 * @Project: ProyectoFinal
 * @FileName: Noticias.java
 * @Date: 22 oct 2021
 * 
 * @Author: Ra�l Delgado
 * @Email: raul.delgadosaez2000@gmail.com
 * @GitUser: @Adc531
 * 
 * @Description: Clase noticias. Poder guardar las noticias en un objeto y escribirlo en un fichero
 * 
**/
package Paquete;

import java.io.IOException;

import javax.swing.JOptionPane;

import org.jsoup.*;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class Noticias {
	
     String url, html, periodico;
     
     
	public Noticias(String url, String html, String periodico) {
		this.url = url;
		this.html = html;
		this.periodico = periodico;
	}
	public Noticias(String periodico) {
		this.periodico = periodico;
	}
	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public String getHtml() {
		return html;
	}


	public void setHtml(String html) {
		this.html = html;
	}


	public String getPeriodico() {
		return periodico;
	}


	public void setPeriodico(String periodico) {
		this.periodico = periodico;
	}


	@Override
	public String toString() {
		String texto = "";
		 Document document;
	     Element Webpage;
		try {
			
           document = Jsoup.connect(url).get();
           Webpage = document.select(html).first();

           texto = Webpage.text();

       } catch (IOException e) {
       	JOptionPane.showMessageDialog(null, "UPS... Algo ha salido mal!!" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	}
		return periodico + ": " + texto + "\n";
	}
	public String Periodico() {
		return url + ";:;" + html + ";:;" + periodico + ";:;";
	}
  
}
	

