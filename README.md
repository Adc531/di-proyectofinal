# DI - ProyectoFinal
# Proyecto Final de Desarrollo de Interfaces. 

Aplicación que mande noticias por email al usuario que estén registrados. También ha de tener una ventana de administrador

# Consultores
1. USUARIOS [1-3 + Administrador]
    - Se tiene que registrar. Usuario y Contraseña. No se puede repetir(Usuario y Contraseña).
    - Cada usuario tiene su propia configuración. (Categoria 1-3. Periodico o web entre las categorias mostradas). Una vez el usuario haya    configurado no lo podra modificar
    - Noticias(Solo el titular).
2. ADMINISTRADOR
    - Gestionar usuarios. (Dar de baja usuarios, modificar noticias).
    - Gestionar noticias. (Actualizar urls de los periodicos, Cambio de hora*)
    - Testear configuración. (Consulta los titulares[Opcion de enviar noticias por email]).


# LIBRERIAS UTILIZADAS
    - Jsoup -> Busqueda de elementos HTML
    - Javax.email -> Envio de correos


