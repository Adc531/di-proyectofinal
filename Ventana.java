/**
 * @Project: ProyectoFinal
 * @FileName: Ventana.java
 * @Date: 15 oct 2021
 * 
 * @Author: Ra�l Delgado
 * @Email: raul.delgadosaez2000@gmail.com
 * @GitUser: @Adc531
 * 
 * @Description: 
 * 
**/

package Paquete;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
/*import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;*/
import java.io.*;

public class Ventana {

	private JFrame frame;
	private JPasswordField Password;
	private JTextField Usuario;
	File f = new File("./Ficheros/Usuarios.txt");
	private JTextField TFUsuario;
	private JTextField TFEmail;
	private JPasswordField passwordField;
	File f1 = new File("./Ficheros/Configuracion.txt");
	File f2 = new File("./Ficheros/ID.txt");
	Image icono = Toolkit.getDefaultToolkit().getImage("D:\\Descargas\\Periodico.png");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana window = new Ventana();
					window.frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ventana() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 885, 535);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));
		frame.setIconImage(icono);
		
		JPanel PanelProgressBar = new JPanel();
		frame.getContentPane().add(PanelProgressBar, "name_1186687814803200");
		PanelProgressBar.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Cargando...");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 21));
		lblNewLabel.setBounds(356, 11, 503, 236);
		PanelProgressBar.add(lblNewLabel);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(115, 203, 625, 76);
		PanelProgressBar.add(progressBar);
		PanelProgressBar.setLayout(null);
		progressBar.setStringPainted(true);
		
		JPanel PanelLogin = new JPanel();
		frame.getContentPane().add(PanelLogin);
		PanelLogin.setLayout(null);
		PanelLogin.setVisible(false);
		
		JButton btnLogin = new JButton("Acceder");
		btnLogin.setBounds(428, 316, 141, 45);
		PanelLogin.add(btnLogin);
		
		JLabel lblNewLabel_1 = new JLabel("Usuario");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_1.setBounds(333, 124, 52, 25);
		PanelLogin.add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("Contrase\u00F1a");
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_1_1.setBounds(305, 194, 80, 25);
		PanelLogin.add(lblNewLabel_1_1);
		
		Password = new JPasswordField();
		Password.setBounds(420, 198, 149, 20);
		PanelLogin.add(Password);
		
		Usuario = new JTextField();
		Usuario.setBounds(420, 128, 149, 20);
		PanelLogin.add(Usuario);
		Usuario.setColumns(10);
		
		
		
		JPanel PanelRegister = new JPanel();
		frame.getContentPane().add(PanelRegister);
		PanelLogin.setLayout(null);
		PanelRegister.setVisible(false);
		JButton btnRegistrarse = new JButton("Registrarse");
		btnRegistrarse.setBounds(244, 316, 141, 45);
		PanelLogin.add(btnRegistrarse);
		
		PanelRegister.setLayout(null);
		
		TFUsuario = new JTextField();
		TFUsuario.setBounds(395, 147, 228, 20);
		PanelRegister.add(TFUsuario);
		TFUsuario.setColumns(10);
		
		TFEmail = new JTextField();
		TFEmail.setColumns(10);
		TFEmail.setBounds(395, 190, 228, 20);
		PanelRegister.add(TFEmail);
		
		JLabel lblNewLabel_2 = new JLabel("Usuario");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_2.setBounds(309, 150, 46, 14);
		PanelRegister.add(lblNewLabel_2);
		
		JLabel lblNewLabel_2_1 = new JLabel("Email");
		lblNewLabel_2_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_2_1.setBounds(309, 193, 46, 14);
		PanelRegister.add(lblNewLabel_2_1);
		
		JLabel lblNewLabel_2_2 = new JLabel("Contrase\u00F1a");
		lblNewLabel_2_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_2_2.setBounds(278, 238, 77, 14);
		PanelRegister.add(lblNewLabel_2_2);
		
		JButton btnRegistro = new JButton("Registrarse");
		btnRegistro.setBounds(367, 297, 154, 55);
		PanelRegister.add(btnRegistro);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(395, 235, 228, 20);
		PanelRegister.add(passwordField);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setBounds(191, 355, 120, 55);
		PanelRegister.add(btnVolver);
		btnVolver.setVisible(false);
		
		Metodos.CargaProgressBar(progressBar, PanelProgressBar, PanelLogin, f, f1);
		
		JPanel PanelConf = new JPanel();
		PanelConf.setVisible(false);
		PanelConf.setBounds(0, 0, 869, 496);
		frame.getContentPane().add(PanelConf);
		PanelConf.setLayout(null);
		PanelConf.setVisible(false);
		Metodos.Registro(btnRegistro, f, passwordField, TFEmail, TFUsuario, btnVolver, PanelLogin, PanelRegister, f1, f2);
		
		JPanel PanelNoticias = new JPanel();
		frame.getContentPane().add(PanelNoticias, "name_1444595930278900");
		PanelNoticias.setLayout(null);
		
		JTextArea TANoticias = new JTextArea();
		TANoticias.setEditable(false);
		TANoticias.setBounds(47, 52, 755, 301);
		PanelNoticias.add(TANoticias);
		
		Login [] l = new Login[4];
		Configuracion[] Conf = new Configuracion[4];
			Conf[0] = new Configuracion("", null, null, null);
			Conf[1] = new Configuracion("", null, null, null);
			Conf[2] = new Configuracion("", null, null, null);
			Conf[3] = new Configuracion("", null, null, null);
		Metodos.login(l, btnLogin, f, Usuario, Password, PanelLogin, PanelConf, PanelNoticias, btnRegistrarse, PanelRegister, Conf);
		
		JRadioButton rdbtnDeportes = new JRadioButton("DEPORTES");
		rdbtnDeportes.setFont(new Font("Tahoma", Font.BOLD, 17));
		rdbtnDeportes.setBounds(66, 63, 128, 23);
		PanelConf.add(rdbtnDeportes);
		
		JRadioButton rdbtnMarca = new JRadioButton("Marca");
		rdbtnMarca.setFont(new Font("Tahoma", Font.BOLD, 17));
		rdbtnMarca.setBounds(83, 103, 128, 23);
		PanelConf.add(rdbtnMarca);
		
		JRadioButton rdbtnSport = new JRadioButton("Sport");
		rdbtnSport.setFont(new Font("Tahoma", Font.BOLD, 17));
		rdbtnSport.setBounds(83, 142, 128, 23);
		PanelConf.add(rdbtnSport);
		
		
		JRadioButton rdbtnF1 = new JRadioButton("F1");
		rdbtnF1.setFont(new Font("Tahoma", Font.BOLD, 17));
		rdbtnF1.setBounds(83, 189, 128, 23);
		PanelConf.add(rdbtnF1);
		
		JRadioButton rdbtnMundo = new JRadioButton("El Mundo");
		rdbtnMundo.setFont(new Font("Tahoma", Font.BOLD, 17));
		rdbtnMundo.setBounds(371, 189, 128, 23);
		PanelConf.add(rdbtnMundo);
		
		JRadioButton rdbtn20Mins = new JRadioButton("20 Minutos");
		rdbtn20Mins.setFont(new Font("Tahoma", Font.BOLD, 17));
		rdbtn20Mins.setBounds(371, 142, 128, 23);
		PanelConf.add(rdbtn20Mins);
	
		
		JRadioButton rdbtnElPais = new JRadioButton("El Pais");
		rdbtnElPais.setFont(new Font("Tahoma", Font.BOLD, 17));
		rdbtnElPais.setBounds(371, 103, 128, 23);
		PanelConf.add(rdbtnElPais);
	
		
		JRadioButton rdbtnNacional = new JRadioButton("NACIONAL");
		rdbtnNacional.setFont(new Font("Tahoma", Font.BOLD, 17));
		rdbtnNacional.setBounds(354, 63, 128, 23);
		PanelConf.add(rdbtnNacional);
		
		JRadioButton rdbtnThetimes = new JRadioButton("The Times");
		rdbtnThetimes.setFont(new Font("Tahoma", Font.BOLD, 17));
		rdbtnThetimes.setBounds(649, 189, 128, 23);
		PanelConf.add(rdbtnThetimes);
	
		
		JRadioButton rdbtnTheEconomist = new JRadioButton("Economist");
		rdbtnTheEconomist.setFont(new Font("Tahoma", Font.BOLD, 17));
		rdbtnTheEconomist.setBounds(649, 142, 128, 23);
		PanelConf.add(rdbtnTheEconomist);
	
		
		JRadioButton rdbtnNewYorkTimes = new JRadioButton("NY Times");
		rdbtnNewYorkTimes.setFont(new Font("Tahoma", Font.BOLD, 17));
		rdbtnNewYorkTimes.setBounds(649, 103, 128, 23);
		PanelConf.add(rdbtnNewYorkTimes);
		
		JRadioButton rdbtnInternacional = new JRadioButton("INTERNACIONAL");
		rdbtnInternacional.setFont(new Font("Tahoma", Font.BOLD, 17));
		rdbtnInternacional.setBounds(632, 63, 182, 23);
		PanelConf.add(rdbtnInternacional);
		
		JButton btnGuardaConf = new JButton("Guardar Configuracion");
		btnGuardaConf.setBounds(313, 334, 220, 67);
		PanelConf.add(btnGuardaConf);
		
		
		JButton btnVolver1 = new JButton("Volver");
		btnVolver1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PanelConf.setVisible(false);
				PanelLogin.setVisible(true);
			}
		});
		btnVolver1.setBounds(34, 413, 144, 46);
		PanelConf.add(btnVolver1);
		btnVolver1.setVisible(false);
		File fConf = new File("./Ficheros/Configuracion.txt");
		
		JButton btnMostrarNoticias = new JButton("Mostrar Noticias");
		btnMostrarNoticias.setBounds(623, 398, 179, 56);
		PanelNoticias.add(btnMostrarNoticias);
		
		JButton btnlogout = new JButton("Desconectarse");
		btnlogout.setBounds(47, 398, 179, 56);
		PanelNoticias.add(btnlogout);
		
		
		
		JButton btnEnviarEmail = new JButton("Enviar email");
		btnEnviarEmail.setBounds(330, 398, 179, 56);
		PanelNoticias.add(btnEnviarEmail);
		Metodos.ConfNoticias(btnGuardaConf, rdbtnDeportes, rdbtnMarca, rdbtnSport, rdbtnF1, rdbtnNacional, rdbtnElPais, rdbtn20Mins, rdbtnInternacional, rdbtnMundo, rdbtnNewYorkTimes, rdbtnTheEconomist, rdbtnThetimes, Conf, btnVolver1, fConf);
		Metodos.Noticias(fConf, TANoticias, btnMostrarNoticias,btnEnviarEmail, l,  f,  Usuario, Password);
	}
}
