package Paquete;

public class Configuracion{
	String Seccion, p1, p2, p3;
	int id;
	public Configuracion(String seccion, String p1, String p2, String p3) {
		Seccion = seccion;
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
	}
	public String getSeccion() {
		return Seccion;
	}
	public void setSeccion(String seccion) {
		Seccion = seccion;
	}
	public String getP1() {
		return p1;
	}
	public void setP1(String p1) {
		this.p1 = p1;
	}
	public String getP2() {
		return p2;
	}
	public void setP2(String p2) {
		this.p2 = p2;
	}
	public String getP3() {
		return p3;
	}
	public void setP3(String p3) {
		this.p3 = p3;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return Seccion + ";:;" + p1 + ";:;" + p2 + ";:;" + p3 + ";:;" + id;
	}
	

	
}
